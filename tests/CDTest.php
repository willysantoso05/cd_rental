<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CDTest extends TestCase
{
    /**
     *  /cd [GET]
     */
    public function testReturnAllCD(){
        $this->get('cd', []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'success',
            'data' => [
                '*' => [
                    'id_cd',
                    'title',
                    'rate',
                    'category',
                    'quantity',
                    'created_at',
                    'updated_at'
                ]
            ]
        ]);
    }

    /**
     *  /cd/id [GET]
     */
    public function testReturnCertainCD(){
        $this->get('cd/1', []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'success',
            'data' => [
                'id_cd',
                'title',
                'rate',
                'category',
                'quantity',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    /**
     *  /cd [POST]
     */
    public function testAddCD(){
        $parameters = [
            'title' => 'Taylor Swift',
            'rate' => '3.2',
            'category' => 'Pop',
            'quantity' => '3'
        ];

        $this->post('cd', $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'success',
            'status',
            'data' => [
                'title',
                'rate',
                'category',
                'quantity',
                'updated_at',
                'created_at',
                'id_cd'
            ]
        ]);
    }
    
    /**
     *  /cd/id [PUT]
     */
    public function testChangeQuantityCD(){
        $parameters = [
            'quantity' => '3'
        ];

        $this->put('cd/1', $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'success',
            'message'
        ]);
    }

    /**
     *  /cd/id [DELETE]
     */
    public function testDeleteCD(){
        $this->delete('cd/6', [], []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'success',
            'message'
        ]);
    }

}
