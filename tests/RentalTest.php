<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class RentTest extends TestCase
{
    /**
     *  /rent [GET]
     */
    public function testReturnAllRent(){
        $this->get('rent', []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'success',
            'data' => [
                '*' => [
                    'id_rent',
                    'id_cd',
                    'start_date',
                    'return_date',
                    'total_day',
                    'created_at',
                    'updated_at'
                ]
            ]
        ]);
    }

    /**
     *  /rent/id [GET]
     */
    public function testReturnCertainRent(){
        $this->get('rent/1', []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'success',
            'data' => [
                'id_rent',
                'id_cd',
                'start_date',
                'return_date',
                'total_day',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    /**
     *  /Rent [POST]
     */
    public function testAddRent(){
        $parameters = [
            'id_cd' => 1
        ];

        $this->post('rent', $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'success',
            'status',
            'data' => [
                'id_cd',
                'updated_at',
                'created_at',
                'id_rent'
            ]
        ]);
    }
    
    /**
     *  /Rent/id [PUT]
     */
    public function testChangeQuantityRent(){
        $this->put('rent/1', [], []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'success',
            'message',
            'Total price',
            'data' => [
                'id_rent',
                'id_cd',
                'start_date',
                'return_date',
                'total_day',
                'created_at',
                'updated_at'
            ]
        ]);
    }

    /**
     *  /rent/id [DELETE]
     */
    public function testDeleteRent(){
        $this->delete('rent/4', [], []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'success',
            'message'
        ]);
    }

}
