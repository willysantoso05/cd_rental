<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//Return all cd in cd_list
$router->get('/cd', 'CDController@getAllCD');

//Return cd info, identified by id_cd
$router->get('/cd/{id}', 'CDController@getCD');

//Add new cd
$router->post('/cd', 'CDController@addCD');

//Update cd stock on certain cd info, identified by id_cd
$router->put('/cd/{id}', 'CDController@updateCD');

//Delete cd with certain id_cd
$router->delete('/cd/{id}', 'CDController@deleteCD');


//Return all rent
$router->get('/rent', 'RentController@getAllRent');

//return rent info, identified by id_rent
$router->get('/rent/{id}', 'RentController@getRent');

//Add new rent
$router->post('/rent', 'RentController@addRent');

//Update rent for return date 
$router->put('/rent/{id}', 'RentController@updateRent');

//Delete rent
$router->delete('/rent/{id}', 'RentController@deleteRent');
