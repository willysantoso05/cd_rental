FORMAT 1A
HOST : http://localhost:8000/

# CD Rental 

Simple CD-Rental REST API, created with Lumen microframework.

## CD

### CD Collection [/cd]

#### List All CD [GET]

+   Response 200 (application/json)
    + Body
        {
            "success": true,
            "data": [
                {
                    "id_cd": 1,
                    "title": "LAUV",
                    "rate": "10.00",
                    "category": "Electro",
                    "quantity": 5,
                    "created_at": "2020-03-21 13:13:19",
                    "updated_at": "2020-03-21 13:14:48"
                },
                {
                    "id_cd": 2,
                    "title": "Maroon 5",
                    "rate": "1.80",
                    "category": "Pop",
                    "quantity": 1,
                    "created_at": "2020-03-21 13:13:39",
                    "updated_at": "2020-03-21 13:14:44"
                },
                {
                    "id_cd": 3,
                    "title": "Imagine Dragons",
                    "rate": "3.50",
                    "category": "RnB",
                    "quantity": 7,
                    "created_at": "2020-03-21 13:14:02",
                    "updated_at": "2020-03-21 13:15:52"
                }
            ]
        }

#### Add New CD [POST]
+   Request (application/json)
        {
            "title" :  "Taylor Swift",
            "rate" : "2.8",
            "category" : "pop",
            "quantity" : "4"
        }

+ Response 200 (application/json)
    + Body
            {
                "success": true,
                "status": "create new CD succesfully created",
                "data": {
                    "title": "Post Malone",
                    "rate": "5.7",
                    "category": "Pop",
                    "quantity": "3",
                    "updated_at": "2020-03-21 15:07:16",
                    "created_at": "2020-03-21 15:07:16",
                    "id_cd": 5
                }
            }

### CD [/cd/{id}]
A CD object has the following attributes :
- title
- rate
- category
- quantity
- created_at
- updated_at

+ parameters
    + id_cd : 1 (required, number) - ID of the CD in the form of integer

#### Get Certain CD [GET]
+ Response 200 (application/json)
    + Body 
            {
                "success": true,
                "data": {
                    "id_cd": 1,
                    "title": "LAUV",
                    "rate": "10.00",
                    "category": "Electro",
                    "quantity": 4,
                    "created_at": "2020-03-21 13:13:19",
                    "updated_at": "2020-03-21 14:47:36"
                }
            }

#### Update quantity of CD [PUT]
+ Request (application/json)
        {
            "quantity" : "2"
        }
+ Response 200 (application/json)
    + Body
            {
                "success": true,
                "message": "quantity updated"
            }

#### Delete cd [DELETE]
+ Response 200 (application/json)
    + Body 
            {
                "success": true,
                "message": "CD is successfully deleted"
            }

## RENT

### Rent Collection [/rent]

#### List All Rent [GET]

+   Response 200 (application/json)
    + Body
        {
            "success": true,
            "data": [
                {
                    "id_rent": 1,
                    "id_cd": 3,
                    "start_date": "2020-03-17 13:14:37",
                    "return_date": "2020-03-21 14:49:14",
                    "total_day": "4.00",
                    "created_at": "2020-03-21 13:14:37",
                    "updated_at": "2020-03-21 14:49:14"
                },
                {
                    "id_rent": 2,
                    "id_cd": 1,
                    "start_date": "2020-03-21 13:14:41",
                    "return_date": null,
                    "total_day": null,
                    "created_at": "2020-03-21 13:14:41",
                    "updated_at": "2020-03-21 13:14:41"
                },
                {
                    "id_rent": 3,
                    "id_cd": 2,
                    "start_date": "2020-03-21 13:14:44",
                    "return_date": null,
                    "total_day": null,
                    "created_at": "2020-03-21 13:14:44",
                    "updated_at": "2020-03-21 13:14:44"
                },
                {
                    "id_rent": 4,
                    "id_cd": 1,
                    "start_date": "2020-03-21 13:14:48",
                    "return_date": null,
                    "total_day": null,
                    "created_at": "2020-03-21 13:14:48",
                    "updated_at": "2020-03-21 13:14:48"
                }
            ]
        }

#### Add New CD [POST]
+   Request (application/json)
        {
            "id_cd" :  "3",
        }

+ Response 200 (application/json)
    + Body
            {
                "success": true,
                "status": "create new Rent succesfully created",
                "data": {
                    "id_cd": "3",
                    "updated_at": "2020-03-21 14:47:36",
                    "created_at": "2020-03-21 14:47:36",
                    "id_rent": 7
                }
            }

### Rent [/rent/{id}]
A CD object has the following attributes :
- id_cd
- start_date
- return_date
- total_day
- created_at
- updated_at

+ parameters
    + id_rent : 1 (required, number) - ID of the Rent in the form of integer

#### Get Certain CD [GET]
+ Response 200 (application/json)
    + Body 
            {
                "success": true,
                "data": {
                    "id_rent": 1,
                    "id_cd": 3,
                    "start_date": "2020-03-17 13:14:37",
                    "return_date": "2020-03-21 13:15:52",
                    "total_day": "4.00",
                    "created_at": "2020-03-21 13:14:37",
                    "updated_at": "2020-03-21 13:15:52"
                }
            }

#### Update quantity of CD [PUT]
+ Response 200 (application/json)
    + Body
            {
                "success": true,
                "message": "quantity updated",
                "Total price": "$14",
                "data": {
                    "id_rent": 1,
                    "id_cd": 3,
                    "start_date": "2020-03-17 13:14:37",
                    "return_date": "2020-03-21 14:49:14",
                    "total_day": "4.00",
                    "created_at": "2020-03-21 13:14:37",
                    "updated_at": "2020-03-21 14:49:14"
                }
            }

#### Delete cd [DELETE]
+ Response 200 (application/json)
    + Body 
            {
                "success": true,
                "message": "Rent is successfully deleted"
            }