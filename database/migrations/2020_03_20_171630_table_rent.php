<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableRent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rent', function (Blueprint $table) {
            $table->increments('id_rent');
            $table->integer('id_cd')->unsigned();
            $table->foreign('id_cd')->references('id_cd')->on('cd_list');
            $table->timestamp('start_date')->useCurrent();
            $table->timestamp('return_date')->nullable();
            $table->decimal('total_day')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rent');
    }
}
