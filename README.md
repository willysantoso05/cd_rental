# CD Rental

Simple CD-Rental REST API, created with Lumen microframework.

## prerequisite
- PHP version 7.2+
- Apache2 server
- MySQL server

## Run Programs
1. Create database for CD Rental System
2. Configure .env file with the mysql setting
3. Run command :
   ```
   $ composer install
   $ php artisan migrate:fresh
   $ php -S localhost:8000 -t ./public
   ```
4. Open http://localhost:8080


## Documentation

| Function                               | Method | Endpoint   | Body Request                    |
|----------------------------------------|--------|------------|---------------------------------|
| Return all CD in cd_list               | GET    | /cd        | -                               |
| Return CD with certain id              | GET    | /cd/{id}   | -                               |
| Insert new CD row in cd_list           | POST   | /cd        | title, rate, category, quantity |
| Update Quantity for CD with certain id | PUT    | /cd/{id}   | quantity                        |
| Delete CD row with certain id          | DELETE | /cd/{id}   | -                               |
| Return all Rent in rent table          | GET    | /rent      | -                               |
| Return Rent with certain id            | GET    | /rent/{id} | -                               |
| Insert new Rent row in rent table      | POST   | /rent      | id_cd                           |
| Return total price for Rent            | PUT    | /rent/{id} | -                               |
| Delete Rent with certain id            | DELETE | /rent/{id} | -                               |