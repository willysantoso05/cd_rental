<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Rent;
use App\CD;

class RentController extends Controller
{
    public function getAllRent(){
        $Rent_query = Rent::get();

        if($Rent_query){
            return response()->json([
                'success' => true,
                'data' => $Rent_query
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'failed to get data'
            ], 400);
        }
    }

    public function getRent($id){
        $Rent_query = Rent::where('id_rent', $id)->first();

        if($Rent_query){
            return response()->json([
                'success' => true,
                'data' => $Rent_query
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'failed to get data'
            ], 400);
        }
    }

    public function addRent(Request $request){
        $id_cd = $request->id_cd;

        //Check stock of the cd with the match id_cd
        $CD_query = CD::where('id_cd', $id_cd)->first();

        if(!$CD_query){
            return response()->json([
                'success' => false,
                'status' => 'create new Rent failed',    
                'data' => ''
            ], 400);
        } else {
            if ($CD_query-> quantity >0){

                //Stock is available, create new rent
                $Rent_query = Rent::create([
                    'id_cd' => $id_cd,
                ]);
        
                if($Rent_query){

                    //Update Stock for the certain cd -> quantity -1
                    $Update_CD_STOCK = CD::where('id_cd', $id_cd)->update(['quantity' => (($CD_query-> quantity) - 1)]);

                    return response()->json([
                        'success' => true,
                        'status' => 'create new Rent succesfully created',
                        'data' => $Rent_query
                    ], 200);
                } else {
                    return response()->json([
                        'success' => false,
                        'status' => 'create new Rent failed',    
                        'data' => ''
                    ], 400);
                }
            } else {
                return response()->json([
                    'success' => false,
                    'status' => 'create new Rent failed, CD is out of stock',    
                    'data' => ''
                ], 400);
            }
        }

    }

    public function updateRent(Request $request, $id){
        //Update return date
        $Rent_query = Rent::where('id_rent', $id)->update(['return_date' => Carbon::now()]);

        //Update total price
        $Rent_price = Rent::where('id_rent', $id)->update(['total_day' => DB::raw('DATEDIFF(return_date, start_date)')]);

        //Get rent result
        $Rent_result = Rent::where('id_rent', $id)->first();

        //Get cd info from rent
        $CD_query = CD::where('id_cd', $Rent_result->id_cd)->first();

        //Update cd stock
        $Update_CD_STOCK = CD::where('id_cd', $Rent_result->id_cd)->update(['quantity' => (($CD_query-> quantity) + 1)]);


        //Calculate price
        $total_price = (($Rent_result->total_day)+1) * $CD_query->rate;


        if($Rent_query && $Rent_result && $CD_query && $Update_CD_STOCK && $total_price){
            return response()->json([
                'success' => true,
                'message' => 'quantity updated',
                'Total price' => '$'.$total_price,
                'data' => $Rent_result
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'quantity is failed to be updated'
            ], 400);
        }
    }

    public function deleteRent($id){
        $Rent_query = Rent::where('id_rent', $id)->delete();

        if($Rent_query){
            return response()->json([
                'success' => true,
                'message' => 'Rent is successfully deleted'
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Rent is failed to be deleted'
            ], 400);
        }
    }
    
}
