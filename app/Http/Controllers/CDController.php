<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CD;

class CDController extends Controller
{
    public function getAllCD(){
        $CD_query = CD::get();

        if($CD_query){
            return response()->json([
                'success' => true,
                'data' => $CD_query
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'failed to get data'
            ], 400);
        }
    }

    public function getCD($id){
        $CD_query = CD::where('id_cd', $id)->first();

        if($CD_query){
            return response()->json([
                'success' => true,
                'data' => $CD_query
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'failed to get data'
            ], 400);
        }
    }

    public function addCD(Request $request){

        $title = $request->title;
        $rate = $request->rate;
        $category = $request->category;
        $quantity = $request->quantity;

        $CD_query = CD::create([
            'title' => $title,
            'rate' => $rate,
            'category' => $category,
            'quantity' => $quantity
        ]);

        if($CD_query){
            return response()->json([
                'success' => true,
                'status' => 'create new CD succesfully created',
                'data' => $CD_query
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'status' => 'create new CD failed',    
                'data' => ''
            ], 400);
        }
    }

    public function updateCD(Request $request, $id){
        $quantity = $request->quantity;

        $CD_query = CD::where('id_cd', $id)->update(['quantity' => $quantity]);

        if($CD_query){
            return response()->json([
                'success' => true,
                'message' => 'quantity updated'
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'quantity is failed to be updated'
            ], 400);
        }
    }

    public function deleteCD($id){
        $CD_query = CD::where('id_cd', $id)->delete();

        if($CD_query){
            return response()->json([
                'success' => true,
                'message' => 'CD is successfully deleted'
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'CD is failed to be deleted'
            ], 400);
        }
    }
    
}
