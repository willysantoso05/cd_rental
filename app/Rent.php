<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    protected $table = 'rent';
    protected $primaryKey = 'id_rent';

    protected $fillable = [
        'id_cd'
    ];
}
