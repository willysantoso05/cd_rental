<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CD extends Model
{
    protected $table = 'cd_list';
    protected $primaryKey = 'id_cd';

    protected $fillable = [
        'title', 'rate', 'category', 'quantity'
    ];

}
